extends Control

@onready var label : Label = $"Current joke"
@onready var laugh_o_meter : ProgressBar = $ProgressBar
@onready var video_player : VideoStreamPlayer = $VideoStreamPlayer

@export var input1 : Control
@export var input2 : Control
@export var input3 : Control
@export var input4 : Control
@export var playground : Node2D


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	label.text = Global.current_joke
	laugh_o_meter.value = Global.laugh_o_meter_charge
	

func set_video_player():
	
	if Global.laugh_o_meter_charge >= 20 && Global.laugh_o_meter_charge < 35:
		video_player.stream.file = "res://Videos/Sus.ogg"
	if Global.laugh_o_meter_charge >= 35 && Global.laugh_o_meter_charge < 50:
		video_player.stream.file = "res://Videos/Grunt.ogg"		
	if Global.laugh_o_meter_charge >= 50 && Global.laugh_o_meter_charge < 80:
		video_player.stream.file = "res://Videos/Jeer.ogg"		
	if Global.laugh_o_meter_charge >= 80:
		video_player.stream.file = "res://Videos/Laugh.ogg"		
	if Global.laugh_o_meter_charge < 20:
		video_player.stream.file = "res://Videos/Asleep.ogg"
	
	video_player.play()	
	
func _on_shuffle_pressed():
	input1.reset()
	input2.reset()
	input3.reset()
	input4.reset()


func _on_clear_pressed():
	Global.current_joke = ""
	playground.clear()


func _on_timer_timeout():
	if Global.laugh_o_meter_charge > 0 || Global.laugh_o_meter_charge < 100:
		Global.laugh_o_meter_charge -= 0.1
