extends Node2D

@onready var ragdoll_pos = $"Ragdoll spawn"
@onready var car_pos = $"Car spawn"
@onready var reboot = $"System override"

var ragdoll = preload("res://Nodes/ragdoll.tscn")
var uncle = preload("res://Nodes/uncle.tscn")
var car = preload("res://Nodes/car.tscn")
var dough = preload("res://Nodes/dough.tscn")

var instance
var car_instance
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Global.laugh_o_meter_charge >= 100:
		reboot.show()

func clear():
	for child in ragdoll_pos.get_children():
		child.queue_free()
	

	for child in car_pos.get_children():
		child.queue_free()	

func create_object():
	if Global.current_joke == "El ragdoll ":
		instance = ragdoll.instantiate()
		ragdoll_pos.add_child(instance)
	elif Global.current_joke == "Tenía un tío que ":
		instance = uncle.instantiate()
		ragdoll_pos.add_child(instance)
	elif Global.current_joke == "Murio arrollado" || Global.current_joke == "El ragdoll Murio arrollado" || Global.current_joke == "Tenía un tío que se llamaba Pio y le deciamos nono. Murio arrollado" || Global.current_joke == "El ragdoll se llamaba Pio y le deciamos nono. Murio arrollado":
		car_instance = car.instantiate()	
		car_pos.add_child(car_instance)
	elif Global.current_joke == "¿Probaste las empanadas sin relleno? Son una masa":
		instance = dough.instantiate()
		ragdoll_pos.add_child(instance)	
		
		
func set_property_to_object():
	if Global.current_joke == "El ragdoll es gigante" || Global.current_joke == "Tenía un tío que es gigante":
		instance.scale_ragdoll(ragdoll_pos.position)
	if Global.current_joke == "El ragdoll se llamaba Pio " || Global.current_joke == "Tenía un tío que se llamaba Pio ":
		instance.show_name()
	if Global.current_joke == "El ragdoll explota" || Global.current_joke == "Tenía un tío que explota":
		instance.explode()
	if Global.current_joke == "El ragdoll se llamaba Pio y le deciamos nono. " || Global.current_joke == "Tenía un tío que se llamaba Pio y le deciamos nono. ":
		instance.update_name()				
