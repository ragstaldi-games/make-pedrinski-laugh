extends Control

var texts = ["¿Probaste ", "las empanadas sin relleno?", " Son una masa", "El ragdoll ", "explota", "es gigante", "come milanesas", "Tenía un tío que ", "se llamaba Pio ", "y le deciamos nono. ", "Murio arrollado"]

var current_text

@onready var button : Button = $Button

# Called when the node enters the scene tree for the first time.
func _ready():
	var random_number = randi() % 11
	current_text = texts[random_number]
	button.text = current_text


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func reset():
	_ready()

func _on_button_pressed():
	Global.set_current_joke(current_text)
	get_parent().get_node("Playground").create_object()
	get_parent().get_node("Playground").set_property_to_object()
	get_parent().get_node("UI").set_video_player()	
	_ready()
