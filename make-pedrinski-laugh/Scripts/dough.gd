extends Node2D

@onready var rb : RigidBody2D = $RigidBody2D
@onready var display = $Display

var bread_sprite = load("res://Assets/Bread.png")
var pizza_sprite = load("res://Assets/Pizza.png")

var mouse_in = false

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func _input(event):
	if mouse_in == true && event.is_action_pressed("Click"):
		if Global.current_joke == "":
			Global.current_joke = "La masa"
		else:
			display.show()

func _on_rigid_body_2d_mouse_entered():
	mouse_in = true


func _on_rigid_body_2d_mouse_exited():
	mouse_in = false


func _on_option_1_pressed():
	display.hide()
	Global.set_current_joke(" " +$Display/Option1.text)
	$RigidBody2D.apply_impulse(Vector2.RIGHT * 200)


func _on_option_2_pressed():
	display.hide()
	Global.set_current_joke(" " +$Display/Option2.text)


func _on_option_3_pressed():
	display.hide()
	Global.set_current_joke(" " +$Display/Option3.text)
	$RigidBody2D/Sprite2D.texture = bread_sprite


func _on_option_4_pressed():
	display.hide()
	Global.set_current_joke(" " +$Display/Option4.text)
	$RigidBody2D/Sprite2D.texture = pizza_sprite
