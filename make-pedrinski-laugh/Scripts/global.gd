extends Node

var current_joke : String

var posible_jokes = ["¿Probaste las empanadas sin relleno? Son una masa", "El ragdoll explota", "El ragdoll es gigante", "El ragdoll come milanesas", "Tenía un tío que se llamaba Pio y le deciamos nono. Murio arrollado", "El ragdoll Murio arrollado", "Tenía un tío que come milanesas", "Tenía un tío que explota", "El ragdoll se llamaba Pio"]

var laugh_o_meter_charge = 0

var repetiton_array = [0,0,0,0,0,0,0,0,0]


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if current_joke == posible_jokes[0]:
		repetiton_array[0] += 1
		laugh_o_meter_charge += 25 / repetiton_array[0]
		current_joke = ""
	elif current_joke == posible_jokes[1]:
		repetiton_array[1] += 1
		laugh_o_meter_charge += 10 / repetiton_array[1]
		current_joke = ""
	elif current_joke == posible_jokes[2]:
		repetiton_array[2] += 1
		laugh_o_meter_charge += 10 / repetiton_array[2]
		current_joke = ""
	elif current_joke == posible_jokes[3]:
		repetiton_array[3] += 1
		laugh_o_meter_charge += 10 / repetiton_array[3]
		current_joke = ""
	elif current_joke == posible_jokes[4]:
		repetiton_array[4] += 1
		laugh_o_meter_charge += 25 / repetiton_array[4]	
		current_joke = ""			
	elif current_joke == posible_jokes[5]:
		repetiton_array[5] += 1
		laugh_o_meter_charge += 20 / repetiton_array[5]
		current_joke = ""
	elif current_joke == posible_jokes[6]:
		repetiton_array[6] += 1
		laugh_o_meter_charge += 10 / repetiton_array[6]
		current_joke = ""
	elif current_joke == posible_jokes[7]:
		repetiton_array[7] += 1
		laugh_o_meter_charge += 10 / repetiton_array[7]
		current_joke = ""
	elif current_joke == posible_jokes[8]:
		repetiton_array[8] += 1
		laugh_o_meter_charge += 10 / repetiton_array[8]	
		current_joke = ""					
	
	if current_joke == "El ragdoll hace pizza y le da al tío, pero no le gusta y mata al ragdoll":
		laugh_o_meter_charge += 50
		current_joke = ""
	
	if current_joke == "El ragdoll hace pan y le da al tío. Lo come, pero muere porque es celiaco":
		laugh_o_meter_charge += 50
		current_joke = ""
	
	if current_joke == "El ragdoll patea la masa y le pega al tío en la cara":
		laugh_o_meter_charge += 30
		current_joke = ""
		
	if current_joke == "El ragdoll come la masa y el tío muere de tristeza porque no le compartieron":
		laugh_o_meter_charge += 30
		current_joke = ""	

func set_current_joke(phrase : String):
	current_joke = current_joke + phrase
	
