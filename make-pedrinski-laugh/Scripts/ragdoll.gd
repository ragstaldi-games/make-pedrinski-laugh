extends Node2D

@onready var ragdoll_name : Label = $Head/Label
@onready var body : RigidBody2D = $Body
@onready var display : Control = $Display



var mouse_in = false

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
	
func _input(event):
	if mouse_in == true && event.is_action_pressed("Click"):
		if Global.current_joke == "":
			Global.current_joke = "El ragdoll"
		else:
			display.show()		
			

func scale_ragdoll(pos):
	apply_scale(Vector2(2,2))
	position = pos

func show_name():
	ragdoll_name.show()
	
func update_name():
	ragdoll_name.text += "'nono'"	
	
func explode():
	body.apply_impulse(Vector2.UP * 2000)
	remove_child($Joints)	


func _on_body_mouse_entered():
	mouse_in = true
	print_debug("Mouse entered")


func _on_body_mouse_exited():
	mouse_in = false
	print_debug("Mouse exited")


func _on_option_1_pressed():
	Global.set_current_joke(" " +$Display/Option1.text+ " " + name)
	display.hide()


func _on_option_2_pressed():
	Global.set_current_joke(" " +$Display/Option2.text+ " " + name)
	display.hide()


func _on_option_3_pressed():
	Global.set_current_joke(" " +$Display/Option3.text + " " + name)
	display.hide()


func _on_option_4_pressed():
	Global.set_current_joke(" " + $Display/Option4.text+ " " + name)
	display.hide()
