extends Node2D
@onready var ragdoll_name : Label = $Head/Label
@onready var body : RigidBody2D = $Body
@onready var display : Control = $Display
@onready var smashed_dough : Sprite2D = $Head/sd



var mouse_in = false

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
	
func _input(event):
	if mouse_in == true && event.is_action_pressed("Click"):
		if Global.current_joke == "":
			Global.current_joke = "El tío"
		else:
			display.show()		
			

func scale_ragdoll(pos):
	apply_scale(Vector2(2,2))
	position = pos

func show_name():
	ragdoll_name.show()
	
func update_name():
	ragdoll_name.text += "'nono'"	
	
func explode():
	body.apply_impulse(Vector2.UP * 2000)
	remove_child($Joints)	

func _on_body_mouse_entered():
	mouse_in = true


func _on_body_mouse_exited():
	mouse_in = false


func _on_option_1_pressed():
	display.hide()
	Global.set_current_joke(" " +$Display/Option1.text)
	explode()


func _on_option_2_pressed():
	display.hide()
	Global.set_current_joke(" " +$Display/Option2.text)
	get_parent().get_node("Ragdoll").explode()


func _on_option_3_pressed():
	display.hide()
	Global.set_current_joke(" " +$Display/Option3.text)
	smashed_dough.show()


func _on_option_4_pressed():
	display.hide()
	Global.set_current_joke(" " +$Display/Option4.text)
