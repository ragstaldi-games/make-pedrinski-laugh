extends Node

var game = preload("res://Nodes/game.tscn")
var menu = preload("res://Nodes/menu.tscn")
var shutdown = preload("res://Nodes/shutdown.tscn")

@onready var timer : Timer = $Timer


var instance


# Called when the node enters the scene tree for the first time.
func _ready():
	instance = menu.instantiate()
	add_child(instance)


func start_game():
	remove_child(instance)
	instance = game.instantiate()
	add_child(instance)

func shutdown_scene():
	remove_child(instance)
	instance = shutdown.instantiate()
	add_child(instance)
	timer.start()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
	



func _on_timer_timeout():
	remove_child(instance)
	_ready()
